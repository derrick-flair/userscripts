# Flair Userscripts

Userscripts are designed to make life easier. While some scripts only save a few seconds (QuickStructure), a few clicks (Auto-Login), other may save hours (Temperature). They are not required, though you may find help in them.


# Installation

[Tampermonkey [Most Browsers]](https://www.tampermonkey.net/) OR  [Greasemonkey [FF Only]](https://www.greasespot.net/)

1. If you want to manually update, or just want to use another tool for your userscripts, you may do so~~; but, only Tampermonkey supports updates.~~

2. Click on each of the following scripts, then click [Install].

# Updating

1. Auto-update: ~~Tampermonkey is currently the only supported auto-updater.~~ Auto-updates are unavailable as of transition to bitbucket.

2. Manual: Click on each of the following scripts, then click [Reinstall].

## Available Userscripts:

**Note: You may use all at the same time as they will only load on the pages for where they are needed.**

#### [Flair Zendesk [QuickStructure]](../../raw/master/FlairZendesk-QuickStructure.user.js)

> Creates a link next to the users's email in the zendesk page to allow looking up their structure lists in [admin.flair.co](admin.flair.co).

#### [Flair Zendesk [QuietMerge]](../../raw/master/FlairZendesk-QuietMerge.user.js)

> Automatically unchecks the "email user of merge" when merging tickets; manually checking the "email user of merge" is still available.

#### [Flair Zendesk [ImportantComment]](../../raw/master/FlairZendesk-ImportantComment.user.js)

> Displays a notification at the top-right of the app-list, when viewing a ticket, if something is filled in for the Comments section (on the left side of ticket).

#### [Flair My [Temperature]](../../raw/master/FlairMy-Temperature.user.js)

> Automatically converts temperatures, including minisplits (4 digit celcius), into both celcius and fahranheit; both are displayed.

#### [Flair Admin [Auto-Login]](../../raw/master/FlairAdmin-AutoLogin.user.js)

> Automatically login to [admin.flair.co](admin.flair.co) if authenticated. Will also redirect when using *Flair Zendesk [QuickStructure]*.

### Change Requests

You may fork for changes and request a pull, or direct message Derrick through Slack for request of changes.

# License
![](https://mirrors.creativecommons.org/presskit/icons/cc.svg?ref=chooser-v1) ![](https://mirrors.creativecommons.org/presskit/icons/by.svg?ref=chooser-v1) ![](https://mirrors.creativecommons.org/presskit/icons/nd.svg?ref=chooser-v1)

[Flair Userscripts](https://bitbucket.org/derrick-flair/userscripts/) by [Derrick Alvarado](https://protechino.com) is licensed under [CC BY-ND 4.0](https://creativecommons.org/licenses/by-nd/4.0?ref=chooser-v1)
