// ==UserScript==
// @name    Flair Zendesk [QuickStructure]
// @version 1.4.3
// @match   https://flair.zendesk.com/*
// @updateURL https://bitbucket.org/derrick-flair/userscripts/raw/master/FlairZendesk-QuickStructure.user.js
// @downloadURL https://bitbucket.org/derrick-flair/userscripts/raw/master/FlairZendesk-QuickStructure.user.js
// @run-at document-start
// ==/UserScript==

function FlairZendesk_QuickStructure() {
  try {
      var emails = document.getElementsByClassName("email");
      for (let email of emails) {
          try {
              if(email.className.startsWith("email") && email.href.startsWith("mailto:") && !email.hasAttribute("userscript")) {
                  var qs_div = document.createElement('span');
                  qs_div.innerHTML = " [<a href=\"https://admin.flair.co/#/filtered-structures/users/" + email.innerHTML.replace(/[\s,<>|!\\/~`'";:&]/g, '') + "\" target=\"_blank\">Structure Lookup</a>]";
                  email.appendChild(qs_div);
                  email.setAttribute("userscript", true);
              }
          } catch(err) {
             //not a proper email
          }
      }
  } catch(err) {
      // no emails available to parse
  }
}

setInterval(FlairZendesk_QuickStructure, 1000);