// ==UserScript==
// @name    Flair Zendesk [ImportantComment]
// @version 1.4.1
// @match   https://flair.zendesk.com/*
// @updateURL https://bitbucket.org/derrick-flair/userscripts/raw/master/FlairZendesk-ImportantComment.user.js
// @downloadURL https://bitbucket.org/derrick-flair/userscripts/raw/master/FlairZendesk-ImportantComment.user.js
// @run-at document-idle
// ==/UserScript==

function FlairZendesk_ImportantComment() {
  try {
      var comment = null;
      var divs = document.getElementsByTagName("div");
      for (let div of divs) {
          if(div.textContent.startsWith("Comments")) {
              comment = div.textContent.replace("Comments", "");
              break;
          }
      }
      if(comment.length > 0) {
          var zd_ic = document.getElementById("zd_important_comment");
          var app_cont = document.getElementsByClassName("app_container")[0];
          var app_cf = "<div style=\"width: 100%; height: auto; min-height: 40px; margin-left: 10px; margin-right: 10px; margin-bottom: 10px;\">";
          app_cf += "<header style=\"background-color: #b0c4de; text-align: center; padding-top: 15px; padding-bottom: 15px; margin-bottom: 7px;\"><span style=\"font-weight: bold; font-size: 500;\">Comments Added</span></header>";
          app_cf += "</div>";
          if(!zd_ic) {
              var cf_div = document.createElement('div');
              cf_div.style = "display: block; order: 9999; margin-right: 5px;";
              cf_div.setAttribute("id", "zd_important_comment");
              cf_div.innerHTML = app_cf;
              app_cont.prepend(cf_div);
          }
      } else {
          var zd_ic_nc = document.getElementById("zd_important_comment");
          zd_ic_nc.remove();
      }
  } catch(err) {
    // ticket not loaded
  }
}

setInterval(FlairZendesk_ImportantComment, 15*1000);