// ==UserScript==
// @name     Flair My [Temperature]
// @version  1.11.7
// @match    https://my.flair.co/h/*
// @updateURL https://bitbucket.org/derrick-flair/userscripts/raw/master/FlairMy-Temperature.user.js
// @downloadURL https://bitbucket.org/derrick-flair/userscripts/raw/master/FlairMy-Temperature.user.js
// @run-at document-idle
// ==/UserScript==

function FlairMy_Temperature() {
  var roundToC = 3;
  var roundToF = 2;

  // Celsius to Fahrenheit
  function getTf(Tc) {
    var Tf = (9/5)*Tc+32;
    return Tf.toFixed(roundToF);
  }

  // Fahrenheit to Celsius
  function getTc(Tf) {
    var Tc = (Tf - 32) / (9/5);
    return Tc.toFixed(roundToC);
  }

  function isValidPage() {
    var validCount = 0;
    var validPages = ["IR Device Log", "Structure Log", "Room Log", "Vent Log", "Bridge Log", "Puck Log", "Thermostat Log", "Geofence Log", "Weather Reading Log"];
    var validPage = false;
    var divs = document.getElementsByTagName("div");
    for(var i = 0; i < divs.length; i++){
    	if(validPages.includes(divs[i].innerHTML)) {
        validCount += 1;
      }
    }
    return (validCount > 1) ? true : false;
  }

  try {
      if(isValidPage()) {
          var hasTargetTemperature = false;
          var hasLongTemperature = false;
          var ignoreColumns = [];
          var invalidColumns = ["humidity", "created-at",
                                "zip-code", "wind-direction",
                                "windspeed", "cloud-cover", "pressure",
                                "bluetooth-tx-power-mw", "becaon-interval-ms",
                                "demo-mode", "firmware-version-b",
                                "firmware-version-s", "firmware-version-w",
                                "gateway-token"];
          var validColumns = ["temperature", "set-point-c", "lower-setpoint-c", "outside-temperature-c"];
          var ths = document.getElementsByTagName("th");
          var tds = document.getElementsByTagName("td");
          for(var i = 0; i < ths.length; i++){
              hasTargetTemperature = false;
              hasLongTemperature = false;
              if(invalidColumns.includes(ths[i].innerHTML)) {
                  ignoreColumns.push(i+1);
              } else if(ths[i].innerHTML.includes("desired-temperature")) {
                  if(!ths[i].innerHTML.includes("F")) {
                      ths[i].innerHTML = ths[i].innerHTML + " (&#176;F)";
                  }
                  hasTargetTemperature = true;
                  hasLongTemperature = true;
              } else if(validColumns.includes(ths[i].innerHTML)) {
                  if(!ths[i].innerHTML.includes("F")) {
                      ths[i].innerHTML = ths[i].innerHTML + " (&#176;F)";
                  }
                  hasTargetTemperature = true;
                  hasLongTemperature = false;
              }

              if(hasTargetTemperature){
                  for(var t = 0; t < tds.length; t++){
                      var TdCol = ((t+1)%ths.length);
                      if(!ignoreColumns.includes(TdCol)) {
                          if(!isNaN(tds[t].innerHTML) && tds[t].innerHTML.length > 0 && !hasLongTemperature){
                              var Temp = tds[t].innerHTML;
                              Temp = parseFloat(Temp);
                              if(Temp >= 40){
                                  tds[t].innerHTML = getTc(Temp) + "&#176;C <br />(" + Temp.toFixed(roundToF) + "&#176;F)";
                              } else {
                                  tds[t].innerHTML = Temp.toFixed(roundToC) + "&#176;C <br />(" + getTf(Temp) + "&#176;F)";
                              }
                          } else if(!isNaN(tds[t].innerHTML) && tds[t].innerHTML.length > 0 && hasLongTemperature){
                              var tmp = tds[t].innerHTML;
                              var tmp_start = tmp.slice(0,2);
                              var tmp_end = tmp.slice(2);
                              var TempFloat = parseFloat(tmp_start+"."+tmp_end);
                              if(TempFloat >= 40){
                                  tds[t].innerHTML = getTc(TempFloat) + "&#176;C <br />(" + TempFloat.toFixed(roundToF) + "&#176;F)";
                              } else {
                                  tds[t].innerHTML = TempFloat.toFixed(roundToC) + "&#176;C <br />(" + getTf(TempFloat) + "&#176;F)";
                              }
                          }
                      }
                  }
                  hasLongTemperature = false;
                  hasTargetTemperature = false;
              }
          }
          ignoreColumns = [];
      }
  } catch(err) {
     console.log(err)
  }
}

setInterval(FlairMy_Temperature, 2500);