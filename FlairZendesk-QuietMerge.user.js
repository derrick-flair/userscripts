// ==UserScript==
// @name    Flair Zendesk [QuietMerge]
// @version 1.2.0
// @match   https://flair.zendesk.com/*
// @updateURL https://bitbucket.org/derrick-flair/userscripts/raw/master/FlairZendesk-QuietMerge.user.js
// @downloadURL https://bitbucket.org/derrick-flair/userscripts/raw/master/FlairZendesk-QuietMerge.user.js
// @run-at document-idle
// ==/UserScript==

function FlairZendesk_QuietMerge() {
  try {
      //disable changes until finished
      var cm = document.getElementById("btn-ticket-confirm-and-merge");
      cm.setAttribute("disabled", "disabled");
      try {
          //set defaults
          var from = document.getElementById("source_is_public");
          var to = document.getElementById("target_is_public");
          if(!from.hasAttribute("userscript") && !to.hasAttribute("userscript")) {
              from.setAttribute("value", 0);
              from.removeAttribute("checked");
              from.setAttribute("userscript", true);
              to.setAttribute("value", 0);
              to.removeAttribute("checked");
              to.setAttribute("userscript", true);
          }
          //add triggers to check if changing, to disable until finsihed
          from.onclick = function() {
              var cm = document.getElementById("btn-ticket-confirm-and-merge");
              var from = document.getElementById("source_is_public");
              cm.setAttribute("disabled", "disabled");
              if(parseInt(from.getAttribute("value")) == 1) {
                  from.setAttribute("value", 0);
                  from.removeAttribute("checked");
              } else {
                  from.setAttribute("value", 1);
                  from.setAttribute("checked", "checked");
              }
              cm.removeAttribute("disabled");
          }
          to.onclick = function() {
              var cm = document.getElementById("btn-ticket-confirm-and-merge");
              var to = document.getElementById("target_is_public");
              cm.setAttribute("disabled", "disabled");
              if(parseInt(to.getAttribute("value")) == 1) {
                  to.setAttribute("value", 0);
                  to.removeAttribute("checked");
              } else {
                  to.setAttribute("value", 1);
                  to.setAttribute("checked", "checked");
              }
              cm.removeAttribute("disabled");
          }
          cm.removeAttribute("disabled");
      } catch(err) {
          //ticket is phone merge
          cm.removeAttribute("disabled");
      }
  } catch(err) {
    // merge not in-use
  }
}

setInterval(FlairZendesk_QuietMerge, 1000);