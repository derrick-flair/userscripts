// ==UserScript==
// @name      Flair Admin [Auto-Login]
// @version   1.4.1
// @match     https://admin.flair.co/*
// @run-at    document-idle
// @updateURL https://bitbucket.org/derrick-flair/userscripts/raw/master/FlairAdmin-AutoLogin.user.js
// @downloadURL https://bitbucket.org/derrick-flair/userscripts/raw/master/FlairAdmin-AutoLogin.user.js
// ==/UserScript==


function FlairAdmin_AutoLogin() {
  var elemA = document.querySelectorAll("a")[5];
  if(elemA.href.includes("oauth")) {
    elemA.innerText = "Loading...";
    var d = new Date();
    d.setTime(d.getTime() + (24*60*60*1000));
    var expires = "expires="+ d.toUTCString();
    var url = window.location.href.replace(/\?.*/,'');
    document.cookie = "redirect_to=" + url + ";SameSite=None;Secure;" + expires + ";path=/";
    window.location.href = elemA.href;
  } else {
    var cookie = "";
    var name = "redirect_to=";
    var decodedCookie = decodeURIComponent(document.cookie);
    var ca = decodedCookie.split(';');
    for(var i = 0; i <ca.length; i++) {
      var c = ca[i];
      while (c.charAt(0) == ' ') {
        c = c.substring(1);
      }
      if (c.indexOf(name) === 0) {
        cookie = c.substring(name.length, c.length);
      }
    }
    if(cookie.length > 3){
      var dc = new Date();
      var cexpires = "expires="+ dc.getTime();
      document.cookie = "redirect_to=;SameSite=None;Secure;" + cexpires + ";path=/";
      window.location.href = cookie;
    }
  }
}

setTimeout(FlairAdmin_AutoLogin, 1500);